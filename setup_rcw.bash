#! /env/bash
DTI_CO_WORKER_ROOT=/opt/rcw
export MODULES_WS_ROOT="$HOME/rcw_ws/src/modules"

export DTI_LANGUAGE=EN

#** DTI Knowledgebase configuration
export DTI_ROBOT_DB_SERVER=localhost
export DTI_ROBOT_DB_DB=dti_co_worker_4.0
export DTI_ROBOT_DB_USER=dti_robot
export DTI_ROBOT_DB_PASSWORD=sG1Qh70U1O
export DTI_ROBOT_DB_PORT=3306

export ROSCONSOLE_FORMAT="[\${severity}] [\${time}] [\${logger}]: \${message}"

source ~/rcw_ws/devel/setup.bash

export ROS_PACKAGE_PATH=$MODULES_WS_ROOT:$ROS_PACKAGE_PATH
