-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Vært: localhost
-- Genereringstid: 29. 06 2018 kl. 16:43:11
-- Serverversion: 5.7.22-0ubuntu0.16.04.1
-- PHP-version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dti_co_worker_4.0`
--
USE `dti_co_worker_4.0`

ALTER TABLE `process_launch_files` ADD `register_module_node` VARCHAR(256) NOT NULL DEFAULT '' AFTER `order_num`;


CREATE TABLE IF NOT EXISTS `launch_file_parameter` (
  `module_parameter_uuid` varchar(36) NOT NULL DEFAULT '(SELECT uuid())',
  `launch_uuid` varchar(36) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(256) NOT NULL,
  `type` varchar(15) NOT NULL,
  `type_name` varchar(256) NOT NULL,
  `node_ref` varchar(256) NOT NULL,
  `datatype` varchar(10) NOT NULL,
  `value` varchar(256) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `order_num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `launch_file_parameter`
  ADD PRIMARY KEY (`module_parameter_uuid`),
  ADD KEY `launch_uuid` (`launch_uuid`);


ALTER TABLE `launch_file_parameter`
  ADD CONSTRAINT `launch_file_parameter_ibfk_1` FOREIGN KEY (`launch_uuid`) REFERENCES `process_launch_files` (`launch_uuid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
