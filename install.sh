#!/bin/bash

set -e

# RCW Initial Install Script
# Should only be used for installing Robot CoWorker on a mint machine, the rest should be done with rodeps and the rcw commands

# Author: Bjarke Kirkelykke Skj�lstrup, based on previous install script by Mikkel Rath Pedersen

# Customized for binary installation

UBUNTU_VERSION=`lsb_release -rs`
if [ ! $UBUNTU_VERSION == "16.04" ]; then
	echo "This install scripts only works for Ubuntu 16.04. Sorry."
	return 1
fi

echo "** This is the Robot CoWorker Install Script - Welcome!"
echo "   This script does the following:"
echo "   - Installs ROS kinetic, and a few additional required tools"
echo "   - Install Robot CoWorker custom commands and rosdep definition"
echo "   - Sets the base location for Robot CoWorker workspaces"
echo ""

echo "** Elevating user rights..."
sudo echo "foo" >> /dev/null
echo ""

echo "** Install ROS kinetic Desktop-Full"
# ROS Installation
sudo sh -E -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
wget -q http://packages.ros.org/ros.key -O - | sudo apt-key add -
# sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116 # <-- from ros.org
sudo -E apt-get -q -y  update
sudo -E apt-get -q -y --force-yes install ros-kinetic-desktop-full
echo "" >> ~/.bashrc
echo "# ROS environment:" >> ~/.bashrc
echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
echo ""
source /opt/ros/kinetic/setup.bash


echo "** Install additional ROS tools"
sudo -E apt-get -q -y --force-yes install python-pip python-wstool python-catkin-tools
source ~/.bashrc
sudo -E rosdep init
rosdep update
echo ""


cd /tmp
echo "** Install Google Chrome"
sudo wget -E --no-cache https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
sudo apt-get -y --force-yes -f install

echo "** Install Robot CoWorker ros deps"
# Rosdep
echo -n "* Downloading rosdep install script (install_ros_deps.sh)... "
sudo wget -E --no-cache https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/install_ros_deps.sh
echo "Done!"
echo "* Installing the rosdeps "
sudo chmod +x install_ros_deps.sh
sudo ./install_ros_deps.sh

echo "** Install Robot CoWorker binaries "
echo -n "* Downloading tarball "
sudo wget -E --no-cache https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/tarballs/rcw/rcw.tar.gz
echo "Done!"
echo "* Unpacking, moving and setting up the CoWorker binaries "
tar xvzf rcw.tar.gz
sudo mv rcw /opt/rcw

cd /opt/rcw
sudo wget -E --no-cache https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/rcw_version

sudo ln -s /opt/rcw/share/gui/dist /var/www/html/rcw

echo "# RCW environment:" >> ~/.bashrc
echo "source /opt/rcw/setup.bash" >> ~/.bashrc
echo ""
source /opt/rcw/setup.bash

mkdir -p ~/rcw_ws/src
mkdir -p ~/rcw_ws/src/modules
cd ~/rcw_ws/src/
sudo wget -E --no-cache https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/tarballs/rcw/workcell.tar.gz
tar xvzf workcell.tar.gz
rm -f workcell.tar.gz
cd ../
catkin clean -y
catkin build

echo "" >> ~/.bashrc
echo "# Your ROS source environment:" >> ~/.bashrc
echo "source ~/rcw_ws/setup_rcw.bash" >> ~/.bashrc
echo ""
wget --no-cache https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/setup_rcw.bash
source ~/rcw_ws/setup_rcw.bash

echo "**********  Installation done!  **********"
echo "**********  Open a new terminal before startin RCW **********"
