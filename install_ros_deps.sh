#[source] Installation commands:
  rosdep-source install https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/rosdep/manifests/rcw/ros_patches.rdmanifest
#[pip] Installation commands:
  sudo -H -E pip install -U dxfgrabber
#[apt] Installation commands:
  sudo -H -E apt-get -y install python-prctl
  sudo -H -E apt-get -y install libxerces-c-dev
#[source] Installation commands:
  rosdep-source install https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/rosdep/manifests/rcw/lamp.rdmanifest
#[apt] Installation commands:
  sudo -H -E apt-get -y install ros-kinetic-moveit
  sudo -H -E apt-get -y install libeigen3-dev
  sudo -H -E apt-get -y install libgtest-dev
  sudo -H -E apt-get -y install libmysqlcppconn-dev
  sudo -H -E apt-get -y install libmodbus-dev
#[source] Installation commands:
  rosdep-source install https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/rosdep/manifests/rcw/smile.rdmanifest
  rosdep-source install https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/rosdep/manifests/rcw/rcw_database.rdmanifest
  rosdep-source install https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/rosdep/manifests/rcw/rcw_statistics_database.rdmanifest
#[apt] Installation commands:
  sudo -H -E apt-get -y install python-mysql.connector
  
  sudo -H -E apt-get -y install ros-kinetic-rosbridge-suite
