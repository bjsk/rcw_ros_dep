# README #
OBS! In all these steps you will need to be connected to the internet. (Except for "Start Robot CoWorker" after installation)

### Install Ubuntu 16.04 64 bit ###
Go to http://releases.ubuntu.com/16.04/

Download the latest "Ubuntu 16.04 64-bit PC (AMD64) desktop image"

Burn it to a disk or create a bootable USB, see https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-windows#0

Put it in the machine and install Ubuntu following the guide. (Remember to use the "Install Alongside Windows" if you have a Windows Installation on the same PC)


### Installing the RCW Installation Manager ###
Download https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/debian/rcw-installation-manager_1.0-2.deb

Double click it and press install

#### Debian package install error
If it does not install it is due to the "Ubuntu Software" issue with Third party debian packages in Ubuntu 16.04, see https://bugs.launchpad.net/ubuntu/+source/gnome-software/+bug/1573206. They have not yet fixed it.

##### Solution 1
Install the old software manager "Ubuntu Software Center". 

Open Ubuntu Software.

Search for "Ubuntu Software Center".

Click Install on the result matching the search.

When it is done installing, right click on the rcw-installation-manager_1.0-2.deb -> "Open With" -> "Ubuntu Software Center"

Press install

##### Solution 2
Open a terminal in the folder where the rcw-installation-manager_1.0-2.deb is located.

Either use the `cd` command to go there or just right click on the folder that it is located and press "Open in terminal"

Now install the debian package by writing:

```
sudo dpkg --install rcw-installation-manager_1.0-2.deb 
```

Enter your password when it ask and press enter

### Using the RCW Installation Manager ###
To open the RCW Installation Mananger click the search button in the top left corner or press "Alt+F2".

Search for "RCW" and click the "RCW Installation Manager"

![Scheme](RCW_Installation_Manager_screen.png)

#### First run
The first time the program is running it will setup itself and install missing libraries if they are not already installed.

Therefor you will se a prompte for you password. When you have entered that the program will stall for some time, then shutdown and open again.

##### Adding it to you launch bar
To have easy access to the RCW Installation Manager you can right click it in left menu bar (launcher) and click "Lock to launcher".

#### Installing the Robot CoWorker
If you haven not already installed the Robot CoWorker, the "Install" button will be available. 

Click this to install Robot CoWorker and setup the required things. 

It will ask you once for you password, when this is entered correctly it will take quite to finish.

After installation is done, close the program and start it again.

#### Updateting the Robot CoWorker
If you have already installed the Robot CoWorker, and there is a newer version of the software on the server, the "Update" button will be available. 

Click this to update Robot CoWorker and setup the required things. 

It will ask you once for you password, after that you will have to wait some minutes for it to finish.

#### Starting the Robot CoWorker
When the installation (or update) is done, the "Start CoWorker" button is available.

Pressing this will start the Robot CoWorker software.

To use the GUI open Google Chrome (not firefox) and to got http://localhost