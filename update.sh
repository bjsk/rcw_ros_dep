#!/bin/bash

# RCW Update Script

# Author: Bjarke Kirkelykke Skjølstrup, based on previous install script by Mikkel Rath Pedersen

# Customized for binary installation

set -e

UBUNTU_VERSION=`lsb_release -rs`
if [ ! $UBUNTU_VERSION == "16.04" ]; then
	echo "This update scripts only works for Ubuntu 16.04. Sorry."
	return 1
fi

echo "** Elevating user rights..."
sudo echo "foo" >> /dev/null
echo ""

rcw_version=$(head -n 1 /opt/rcw/rcw_version)
IFS=. read major minor micro <<EOF
${rcw_version##*-}
EOF

sudo rm -rf /tmp/rcw
mkdir /tmp/rcw
cd /tmp/rcw

echo "** Updating Robot CoWorker binaries "
echo -n "* Downloading tarball "
sudo wget -E --no-cache https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/tarballs/rcw/rcw.tar.gz
echo "Done!"
echo "* Unpacking, moving and setting up the CoWorker binaries "
tar xvzf rcw.tar.gz

sudo rm -rf /opt/rcw_prev_ver
sudo mv /opt/rcw /opt/rcw_prev_ver

sudo mv rcw /opt/rcw

cd /opt/rcw
sudo wget -E --no-cache https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/rcw_version

sudo rm -rf /tmp/rcw

if [ $major = 4 ] && [ $minor = 0 ] && [ $micro -lt 4 ]; then
	cd ~/rcw_ws/
	rm setup_rcw.bash
	wget --no-cache https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/setup_rcw.bash
fi

if [ $major = 4 ] && [ $minor = 0 ]; then
	cd /tmp
	wget --no-cache https://bitbucket.org/bjsk/rcw_ros_dep/raw/master/updates/4.1.0/update_to_4.1.0.sh
	sudo chmod +x ./update_to_4.1.0.sh
	sudo ./update_to_4.1.0.sh
	sudo rm update_to_4.1.0.sh
fi

source ~/rcw_ws/setup_rcw.bash

echo "**********  Update done!  **********"
