-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Vært: localhost
-- Genereringstid: 29. 06 2018 kl. 18:22:28
-- Serverversion: 5.7.22-0ubuntu0.16.04.1
-- PHP-version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dti_co_worker_4.0`
--
CREATE DATABASE IF NOT EXISTS `dti_co_worker_4.0` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dti_co_worker_4.0`;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `actions`
--

CREATE TABLE `actions` (
  `uuid` varchar(36) NOT NULL,
  `skill_parent_uuid` varchar(36) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `type` varchar(250) DEFAULT NULL,
  `skill_ref_uuid` varchar(250) DEFAULT NULL,
  `class_type` varchar(32) NOT NULL,
  `node_origin` varchar(256) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `launch_file_parameter`
--

CREATE TABLE `launch_file_parameter` (
  `module_parameter_uuid` varchar(36) NOT NULL DEFAULT '(SELECT uuid())',
  `launch_uuid` varchar(36) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(256) NOT NULL,
  `type` varchar(15) NOT NULL,
  `type_name` varchar(256) NOT NULL,
  `node_ref` varchar(256) NOT NULL,
  `datatype` varchar(10) NOT NULL,
  `value` varchar(256) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `order_num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `parameters`
--

CREATE TABLE `parameters` (
  `uuid` varchar(36) NOT NULL,
  `isOutput` tinyint(1) NOT NULL,
  `isVariable` tinyint(1) NOT NULL DEFAULT '0',
  `parent_skill_uuid` varchar(36) DEFAULT NULL,
  `parent_action_uuid` varchar(36) DEFAULT NULL,
  `nested_from_uuid` varchar(36) DEFAULT NULL,
  `skill_parameter_ref_uuid` varchar(36) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `type` varchar(10) NOT NULL,
  `description` varchar(256) NOT NULL,
  `value` varchar(100) NOT NULL,
  `sortNum` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `process_launch_files`
--

CREATE TABLE `process_launch_files` (
  `launch_uuid` varchar(36) NOT NULL DEFAULT '(SELECT uuid())',
  `proc_uuid` varchar(36) NOT NULL,
  `name` varchar(256) NOT NULL,
  `package` varchar(256) NOT NULL,
  `type` varchar(256) NOT NULL,
  `register_module_node` varchar(256) NOT NULL DEFAULT '',
  `namespace` varchar(256) NOT NULL DEFAULT '/',
  `order_num` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `processes`
--

CREATE TABLE `processes` (
  `proc_uuid` varchar(36) NOT NULL DEFAULT '(SELECT uuid())',
  `proc_name` varchar(256) NOT NULL,
  `system_uuid` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `processes`
--

INSERT INTO `processes` (`proc_uuid`, `proc_name`, `system_uuid`) VALUES
('a0a5afbd-460f-11e7-ae07-54ee758737c1', 'Test Process', '8b2e91ed-4451-11e7-bac7-54ee758737c1'),
('c042cf88-4453-11e7-bac7-54ee758737c1', 'RAMPup Application', '8b2e91ed-4451-11e7-bac7-54ee758737c1');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `skill_logic`
--

CREATE TABLE `skill_logic` (
  `skill_logic_uuid` varchar(36) NOT NULL,
  `skill_parent_uuid` varchar(36) DEFAULT NULL,
  `from_action_uuid` varchar(36) DEFAULT NULL,
  `to_action_uuid` varchar(36) DEFAULT NULL,
  `from_parameter_uuid` varchar(36) DEFAULT NULL,
  `to_parameter_uuid` varchar(36) DEFAULT NULL,
  `connection_type` varchar(4) NOT NULL,
  `cond_structure` varchar(256) NOT NULL,
  `sortNum` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `skill_logic_conditions`
--

CREATE TABLE `skill_logic_conditions` (
  `skill_logic_uuid` varchar(36) NOT NULL,
  `index_num` int(2) NOT NULL,
  `parameter_1_uuid` varchar(36) NOT NULL,
  `operator` varchar(3) NOT NULL,
  `parameter_2_uuid` varchar(36) NOT NULL,
  `data_type` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `skills`
--

CREATE TABLE `skills` (
  `uuid` varchar(36) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(512) NOT NULL,
  `module` varchar(36) DEFAULT '',
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `systems`
--

CREATE TABLE `systems` (
  `system_uuid` varchar(36) NOT NULL DEFAULT '(SELECT uuid())',
  `system_name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `systems`
--

INSERT INTO `systems` (`system_uuid`, `system_name`) VALUES
('8b2e91ed-4451-11e7-bac7-54ee758737c1', 'RAMPup Test System');

--
-- Begrænsninger for dumpede tabeller
--

--
-- Indeks for tabel `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`uuid`),
  ADD UNIQUE KEY `uuid` (`uuid`),
  ADD KEY `type` (`type`),
  ADD KEY `skill_parent_uuid` (`skill_parent_uuid`),
  ADD KEY `skill_ref_uuid` (`skill_ref_uuid`);

--
-- Indeks for tabel `launch_file_parameter`
--
ALTER TABLE `launch_file_parameter`
  ADD PRIMARY KEY (`module_parameter_uuid`),
  ADD KEY `launch_uuid` (`launch_uuid`);

--
-- Indeks for tabel `parameters`
--
ALTER TABLE `parameters`
  ADD PRIMARY KEY (`uuid`),
  ADD UNIQUE KEY `uuid` (`uuid`),
  ADD KEY `parent_skill_uuid` (`parent_skill_uuid`),
  ADD KEY `parent_action_uuid` (`parent_action_uuid`),
  ADD KEY `nested_from_uuid` (`nested_from_uuid`),
  ADD KEY `skill_parameter_ref` (`skill_parameter_ref_uuid`);

--
-- Indeks for tabel `process_launch_files`
--
ALTER TABLE `process_launch_files`
  ADD PRIMARY KEY (`launch_uuid`),
  ADD KEY `launch_uuid` (`launch_uuid`);

--
-- Indeks for tabel `processes`
--
ALTER TABLE `processes`
  ADD PRIMARY KEY (`proc_uuid`),
  ADD KEY `proc_uuid` (`proc_uuid`),
  ADD KEY `system_uuid` (`system_uuid`);

--
-- Indeks for tabel `skill_logic`
--
ALTER TABLE `skill_logic`
  ADD PRIMARY KEY (`skill_logic_uuid`),
  ADD KEY `skill_parent_uuid` (`skill_parent_uuid`),
  ADD KEY `from_action_uuid` (`from_action_uuid`),
  ADD KEY `to_action_uuid` (`to_action_uuid`),
  ADD KEY `from_parameter_uuid` (`from_parameter_uuid`),
  ADD KEY `to_parameter_uuid` (`to_parameter_uuid`);

--
-- Indeks for tabel `skill_logic_conditions`
--
ALTER TABLE `skill_logic_conditions`
  ADD UNIQUE KEY `skill_logic_uuid_3` (`skill_logic_uuid`,`parameter_1_uuid`),
  ADD KEY `skill_logic_uuid` (`skill_logic_uuid`),
  ADD KEY `parameter_uuid` (`parameter_1_uuid`),
  ADD KEY `skill_logic_uuid_2` (`skill_logic_uuid`,`parameter_1_uuid`),
  ADD KEY `parameter_2_uuid` (`parameter_2_uuid`);

--
-- Indeks for tabel `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`uuid`);

--
-- Indeks for tabel `systems`
--
ALTER TABLE `systems`
  ADD PRIMARY KEY (`system_uuid`),
  ADD KEY `system_uuid` (`system_uuid`);

--
-- Begrænsninger for dumpede tabeller
--

--
-- Begrænsninger for tabel `actions`
--
ALTER TABLE `actions`
  ADD CONSTRAINT `actions_ibfk_1` FOREIGN KEY (`skill_parent_uuid`) REFERENCES `skills` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `actions_ibfk_2` FOREIGN KEY (`skill_ref_uuid`) REFERENCES `skills` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Begrænsninger for tabel `launch_file_parameter`
--
ALTER TABLE `launch_file_parameter`
  ADD CONSTRAINT `launch_file_parameter_ibfk_1` FOREIGN KEY (`launch_uuid`) REFERENCES `process_launch_files` (`launch_uuid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrænsninger for tabel `parameters`
--
ALTER TABLE `parameters`
  ADD CONSTRAINT `parameters_ibfk_1` FOREIGN KEY (`parent_skill_uuid`) REFERENCES `skills` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `parameters_ibfk_2` FOREIGN KEY (`parent_action_uuid`) REFERENCES `actions` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `parameters_ibfk_3` FOREIGN KEY (`nested_from_uuid`) REFERENCES `parameters` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `parameters_ibfk_4` FOREIGN KEY (`skill_parameter_ref_uuid`) REFERENCES `parameters` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Begrænsninger for tabel `skill_logic`
--
ALTER TABLE `skill_logic`
  ADD CONSTRAINT `skill_logic_ibfk_1` FOREIGN KEY (`skill_parent_uuid`) REFERENCES `skills` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `skill_logic_ibfk_2` FOREIGN KEY (`from_action_uuid`) REFERENCES `actions` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `skill_logic_ibfk_3` FOREIGN KEY (`to_action_uuid`) REFERENCES `actions` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `skill_logic_ibfk_4` FOREIGN KEY (`from_parameter_uuid`) REFERENCES `parameters` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `skill_logic_ibfk_5` FOREIGN KEY (`to_parameter_uuid`) REFERENCES `parameters` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Begrænsninger for tabel `skill_logic_conditions`
--
ALTER TABLE `skill_logic_conditions`
  ADD CONSTRAINT `skill_logic_conditions_ibfk_1` FOREIGN KEY (`skill_logic_uuid`) REFERENCES `skill_logic` (`skill_logic_uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `skill_logic_conditions_ibfk_2` FOREIGN KEY (`parameter_1_uuid`) REFERENCES `parameters` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `skill_logic_conditions_ibfk_3` FOREIGN KEY (`parameter_2_uuid`) REFERENCES `parameters` (`uuid`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
