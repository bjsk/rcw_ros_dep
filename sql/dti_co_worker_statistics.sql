-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Vært: localhost
-- Genereringstid: 05. 10 2017 kl. 10:49:35
-- Serverversion: 5.7.19-0ubuntu0.16.04.1
-- PHP-version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dti_co_worker_statistics`
--
DROP DATABASE IF EXISTS `dti_co_worker_statistics`;
CREATE DATABASE IF NOT EXISTS `dti_co_worker_statistics` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dti_co_worker_statistics`;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `custom_counters`
--

CREATE TABLE `custom_counters` (
  `counterId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `counter` int(10) UNSIGNED NOT NULL,
  `customIdentifier` varchar(256) NOT NULL,
  `start_sec` int(10) UNSIGNED NOT NULL,
  `start_nsec` int(10) UNSIGNED NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `custom_cycles`
--

CREATE TABLE `custom_cycles` (
  `custom_cycle_id` int(11) NOT NULL,
  `identifier` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `cycle_times`
--

CREATE TABLE `cycle_times` (
  `session_sg_id` int(11) NOT NULL,
  `runtime_sec` int(10) UNSIGNED NOT NULL,
  `runtime_nsec` int(10) UNSIGNED NOT NULL,
  `start_sec` int(10) UNSIGNED NOT NULL,
  `start_nsec` int(10) UNSIGNED NOT NULL,
  `stop_sec` int(10) UNSIGNED NOT NULL,
  `stop_nsec` int(10) UNSIGNED NOT NULL,
  `pause_sec` int(10) UNSIGNED NOT NULL,
  `pause_nsec` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `messages`
--

CREATE TABLE `messages` (
  `session_sg_id` int(11) NOT NULL,
  `sec` int(10) UNSIGNED NOT NULL,
  `nsec` int(10) UNSIGNED NOT NULL,
  `message` text NOT NULL,
  `isBlocking` tinyint(1) NOT NULL,
  `jogAllowed` tinyint(1) NOT NULL,
  `destination` varchar(256) NOT NULL,
  `nsec_epoc` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `process_data`
--

CREATE TABLE `process_data` (
  `session_sg_id` int(11) NOT NULL,
  `sec` int(11) NOT NULL,
  `nsec` int(11) NOT NULL,
  `nsec_epoc` bigint(20) NOT NULL,
  `identifier` varchar(256) NOT NULL,
  `value` varchar(256) NOT NULL,
  `datatype` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `runtime_sessions`
--

CREATE TABLE `runtime_sessions` (
  `session_sg_id` int(11) NOT NULL,
  `sessionId` varchar(30) DEFAULT NULL,
  `skill_uuid` varchar(36) NOT NULL,
  `custom_cycle_id` int(11) DEFAULT NULL,
  `count` int(10) UNSIGNED NOT NULL,
  `averageTimeSec` int(10) UNSIGNED NOT NULL,
  `averageTimeNsec` int(10) UNSIGNED NOT NULL,
  `start_sec` int(10) UNSIGNED NOT NULL,
  `start_nsec` int(10) UNSIGNED NOT NULL,
  `stop_sec` int(10) UNSIGNED NOT NULL,
  `stop_nsec` int(10) UNSIGNED NOT NULL,
  `runtime_sec` int(10) UNSIGNED NOT NULL,
  `runtime_nsec` int(10) UNSIGNED NOT NULL,
  `pause_sec` int(10) UNSIGNED NOT NULL,
  `pause_nsec` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Begrænsninger for dumpede tabeller
--

--
-- Indeks for tabel `custom_counters`
--
ALTER TABLE `custom_counters`
  ADD PRIMARY KEY (`counterId`);

--
-- Indeks for tabel `custom_cycles`
--
ALTER TABLE `custom_cycles`
  ADD PRIMARY KEY (`custom_cycle_id`);

--
-- Indeks for tabel `cycle_times`
--
ALTER TABLE `cycle_times`
  ADD KEY `session_sg_id` (`session_sg_id`),
  ADD KEY `runtime_sec` (`runtime_sec`),
  ADD KEY `runtime_nsec` (`runtime_nsec`),
  ADD KEY `start_sec` (`start_sec`),
  ADD KEY `start_nsec` (`start_nsec`),
  ADD KEY `stop_sec` (`stop_sec`),
  ADD KEY `stop_nsec` (`stop_nsec`),
  ADD KEY `pause_sec` (`pause_sec`),
  ADD KEY `pause_nsec` (`pause_nsec`);

--
-- Indeks for tabel `messages`
--
ALTER TABLE `messages`
  ADD KEY `session_sg_id` (`session_sg_id`),
  ADD KEY `sec` (`sec`),
  ADD KEY `nsec` (`nsec`),
  ADD KEY `nsec_epoc` (`nsec_epoc`);

--
-- Indeks for tabel `process_data`
--
ALTER TABLE `process_data`
  ADD KEY `session_sg_id` (`session_sg_id`),
  ADD KEY `sec` (`sec`),
  ADD KEY `nsec` (`nsec`),
  ADD KEY `nsec_epoc` (`nsec_epoc`);

--
-- Indeks for tabel `runtime_sessions`
--
ALTER TABLE `runtime_sessions`
  ADD PRIMARY KEY (`session_sg_id`),
  ADD KEY `sessionId` (`sessionId`),
  ADD KEY `skillId` (`skill_uuid`),
  ADD KEY `custom_cycle_id` (`custom_cycle_id`),
  ADD KEY `averageTimeSec` (`averageTimeSec`),
  ADD KEY `averageTimeNsec` (`averageTimeNsec`);

--
-- Brug ikke AUTO_INCREMENT for slettede tabeller
--

--
-- Tilføj AUTO_INCREMENT i tabel `custom_counters`
--
ALTER TABLE `custom_counters`
  MODIFY `counterId` int(11) NOT NULL AUTO_INCREMENT;
--
-- Tilføj AUTO_INCREMENT i tabel `custom_cycles`
--
ALTER TABLE `custom_cycles`
  MODIFY `custom_cycle_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Tilføj AUTO_INCREMENT i tabel `runtime_sessions`
--
ALTER TABLE `runtime_sessions`
  MODIFY `session_sg_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Begrænsninger for dumpede tabeller
--

--
-- Begrænsninger for tabel `cycle_times`
--
ALTER TABLE `cycle_times`
  ADD CONSTRAINT `cycle_times_ibfk_1` FOREIGN KEY (`session_sg_id`) REFERENCES `runtime_sessions` (`session_sg_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrænsninger for tabel `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`session_sg_id`) REFERENCES `runtime_sessions` (`session_sg_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrænsninger for tabel `process_data`
--
ALTER TABLE `process_data`
  ADD CONSTRAINT `process_data_ibfk_1` FOREIGN KEY (`session_sg_id`) REFERENCES `runtime_sessions` (`session_sg_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrænsninger for tabel `runtime_sessions`
--
ALTER TABLE `runtime_sessions`
  ADD CONSTRAINT `runtime_sessions_ibfk_1` FOREIGN KEY (`custom_cycle_id`) REFERENCES `custom_cycles` (`custom_cycle_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
